// testecli.cpp : Defines the entry point for the console application.
//
/*
* Para inicializar o programa usar a chamada conforme o exemplo:
* ./testecli -ipsrv 192.168.0.55 -psrv 3000 -pcli 2500
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define	SOCKET	int
#define INVALID_SOCKET  ((SOCKET)~0)


// #define PORTA_CLI 2345 // porta TCP do cliente
// #define PORTA_SRV 3000 // porta TCP do servidor
// //#define STR_IPSERVIDOR "127.0.0.1"
// #define STR_IPSERVIDOR "192.168.0.2"

int main(int argc, char* argv[])
{
  SOCKET s;
  struct sockaddr_in  s_cli, s_serv;
  int porta_cli, porta_srv, i;
  char ip_srv[16], ip_cli[16];

  // abre socket TCP
  if ((s = socket(AF_INET, SOCK_STREAM, 0))==INVALID_SOCKET)
  {
    printf("Erro iniciando socket\n");
    return(0);
  }

  if(argc < 9)
  {
    printf("Utilizar:\n");
    printf("testecli -ipsrv <ip servidor> -ipcli <ip cliente> -psrv <porta servidor> -pcli <porta cliente>\n");
    exit(1);
  }
  else
  {
      for(i=0; i < argc; i +=2)
      {
        if(strcmp(argv[i], "-ipsrv") == 0)
        {
          strcpy(ip_srv, argv[i+1]);
          printf("ipsrv %s\n", ip_srv);
        }
        else if(strcmp(argv[i], "-ipcli") == 0)
        {
          strcpy(ip_cli, argv[i+1]);
          printf("ipcli %s\n", ip_cli);
        }
        else if(strcmp(argv[i], "-psrv") == 0)
        {
          porta_srv = atoi(argv[i+1]);
          printf("porta srv %s\n", ip_srv);
        }
        else if(strcmp(argv[i], "-pcli") == 0)
        {
          porta_cli = atoi(argv[i+1]);
          printf("porta cli %s\n", ip_srv);
        }

      }
    }

  // seta informacoes IP/Porta locais
  s_cli.sin_family = AF_INET;
  s_cli.sin_addr.s_addr = htonl(INADDR_ANY);
  s_cli.sin_port = htons(porta_cli);



  // associa configuracoes locais com socket
  if ((bind(s, (struct sockaddr *)&s_cli, sizeof(s_cli))) != 0)
  {
    printf("erro no bind\n");
    close(s);
    return(0);
  }

  // seta informacoes IP/Porta do servidor remoto
  s_serv.sin_family = AF_INET;
  s_serv.sin_addr.s_addr = inet_addr(ip_srv);
  s_serv.sin_port = htons(porta_srv);

  // connecta socket aberto no cliente com o servidor
  if(connect(s, (struct sockaddr*)&s_serv, sizeof(s_serv)) != 0)
  {
    //printf("erro na conexao - %d\n", WSAGetLastError());
    printf("erro na conexao");
    close(s);
    exit(1);
  }

// #if 0
//   // envia mensagem de conexao - aprimorar para dar IP e porta
//   if ((send(s, "Conectado\n", 11,0)) == SOCKET_ERROR);
//   {
//     printf("erro na transmiss�o - %d\n", WSAGetLastError());
//     closesocket(s);
//     return 0;
//   }
// #endif

  // recebe do teclado e envia ao servidor
  char str[1250];
  char ch;

  srand(time(NULL));

  while(1)
  {
    for(i=0; (i<80) &&  (ch = (rand() % 126 + 35)) != '\n'; i++ )
      str[i] = (char)ch;
    str[i] = '\0';
    
    if ((send(s, (const char *)&str, sizeof(str),0)) < 0)
    {
      printf("erro na transmiss�o\n");
      close(s);
      return 0;
    }

    strcpy(str, "q");

    if(strcmp((const char *)&str, "q")==0)
      break;
     
     //usleep(velocidade);
  }

  // fecha socket e termina programa
  printf("Fim da conexao\n");
  close(s);
  return 0;
}
