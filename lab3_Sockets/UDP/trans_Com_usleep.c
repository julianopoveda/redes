/* Copyright (C) 2006 PRAV - Pesquisa em Redes de Alta Velocidade
 *                    NTVD - N�cleo de TV Digital
 * http://www.ufrgs.br/ntvd
 *
 *  O objetivo deste programa � apresentar a base da estrutura de programa��o com sockets
 *  atrav�s de UDP
 *
 * Cli.c: Esqueleto de cliente UDP.
 * Argumentos: -h <IP destino> -p <porta>
 *
 * Desenvolvido para sistemas UNIX Like (Linux, FreeBSD, NetBSD...) e Windows
 *		Maiko de Andrade
 *		Valter Roesler
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#define SOCKET	int

int main(int argc, char **argv){
	 struct sockaddr_in peer;
	 SOCKET s;
	 int porta, peerlen, i;
	 int velocidade;
	 char ip[16], buffer[1250];

	 if(argc < 6) 
	 {
		printf("Utilizar:\n");
		printf("trans -h <numero_ip> -p <porta> -r <velocidade de transmissao>\n");
		exit(1);
	 }

	 for(i=1; i<argc; i++) 
	 {
		  if(argv[i][0]=='-') 
		  {
				switch(argv[i][1]) 
				{
					 case 'h': // Numero IP
						  i++;
						  strcpy(ip, argv[i]);
						  break;
					 case 'p': // porta
						  i++;
						  porta = atoi(argv[i]);
						  if(porta < 1024) {
								printf("Valor da porta invalido\n");
								exit(1);
						  }
						  break;
				  	 case 'r': //velocidade
							i++;
							velocidade = (1/(atof(argv[i])))*1300000;//de kbits bits. Velocidade mais próxima na minha máquina
						break;

					 default:
						  printf("Parametro invalido %d: %s\n",i,argv[i]);
						  exit(1);
				}
		  } 
		  else 
		  {
			printf("Parametro %d: %s invalido\n",i, argv[i]);
			exit(1);
		  }
	 }
	 
	 if((s = socket(AF_INET, SOCK_DGRAM,0)) < 0) 
	 {
		  printf("Falha na criacao do socket\n");
		  exit(1);
 	 }

	peer.sin_family = AF_INET;
	peer.sin_port = htons(porta);
	peer.sin_addr.s_addr = inet_addr(ip);
	peerlen = sizeof(peer);

	while(1)
	{
		sendto(s, buffer, sizeof(buffer), 0, (struct sockaddr *)&peer, peerlen);
		usleep(velocidade);
	}
}
